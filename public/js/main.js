$(document).ready(function(){
    validLogin();
    validInclusao();    
    $('form').find('input[data-type="number"]').each(function(){
        setMaskNumero($(this));
    })
});

var validLogin = function()
{
    $('div.login form input[type="submit"]').click(function(){
        formOk = true;
        $('form input[type="text"]').each(function(){
            if ($(this).attr('data-must') == 1 && $(this).val() == '') {
                if (formOk) {
                    alert('Necessário preencher todos campos obrigatórios');
                    formOk = false;
                }
            }
        });
        $('form input[type="password"]').each(function(){
            if ($(this).attr('data-must') == 1 && $(this).val() == '') {
                if (formOk) {
                    alert('Necessário preencher todos campos obrigatórios');
                    formOk = false;
                }
            }
        });
        $('form textarea').each(function(){
            if ($(this).attr('data-must') == 1 && $(this).val() == '') {
                if (formOk) {
                    alert('Necessário preencher todos campos obrigatórios');
                    formOk = false;
                }
            }
        });
        var data = $('form').serialize();
        if (formOk) {
            $.ajax({
                url: '/Login/validate',
                data : data,
                type: 'post',
                success: function(data) {
                    if (data.status == false) {
                        alert('Usuário ou senha inválidos');
                    } else {
                        location.href = '/Tarefa/relatorio'
                    }
                }
            })
        }
        
        return false;
    })
}

var validInclusao = function()
{
    $('div.inclusao form input[type="submit"]').click(function(){
        formOk = true;
        $('form input[type="text"]').each(function(){
            if ($(this).attr('data-must') == 1 && $(this).val() == '') {
                if (formOk) {
                    alert('Necessário preencher todos campos obrigatórios');
                    formOk = false;
                }
            }
        });
        $('form input[type="password"]').each(function(){
            if ($(this).attr('data-must') == 1 && $(this).val() == '') {
                if (formOk) {
                    alert('Necessário preencher todos campos obrigatórios');
                    formOk = false;
                }
            }
        });
        $('form textarea').each(function(){
            if ($(this).attr('data-must') == 1 && $(this).val() == '') {
                if (formOk) {
                    alert('Necessário preencher todos campos obrigatórios');
                    formOk = false;
                }
            }
        });
        if (!formOk) {
            return false;
        }
    })
}

var setMaskNumero = function(obj)
{
	obj.keyup(function(){
		obj.val(obj.val().replace(/\D/g,''));	
	})
}
