<?php
/**
* Função de autoloader para aplicação carregar apenas arquivos conforme utilização
*/

session_start();

function __autoloader ($class) {
	$path = str_replace('\\', '/', $class);
	if (file_exists(__DIR__ . '/../' . $path . '.php')) {
		require_once(__DIR__. '/../' . $path . '.php');
	}
}

spl_autoload_register('__autoloader');

$url = substr($_SERVER['REQUEST_URI'], 1);
$args = explode("/", $url);

require_once(__DIR__ . '/../config/Config.php');

if (empty($args[0])) {
	$className = 'App\Controller\Login';
} else {
	$className = 'App\Controller\\'.$args[0];
}

if (empty($args[1])) {
	$action = 'indexAction';
} else {
	$action = $args[1].'Action';
}

$controller = new $className();
$controller->$action();
