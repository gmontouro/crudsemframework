<?php

/**
* Classe para controlar a sessao do usuário, logado, deslogado, etc.
*/

namespace App\Session;

use App\DTO\Usuario;

class Session
{
    /** Caso usuário esteja logado retorna seu DTO, caso contrário retorna nulo
    *   @return $usuario, DTO | null
    */
    public static function read()
    {
        if (!empty($_SESSION['usuario'])) {
            return $_SESSION['usuario'];
        }

        return null;
    }

    /**
    * Grava um usuário na sessão
    * @param $usuario | App\DTO\Usuario
    */
    public static function saveSession(Usuario $usuario)
    {
        $_SESSION['usuario'] = $usuario;
    }

    /**
    * Esvazia a sessão, faz logout
    */
    public static function flushSession()
    {
        $_SESSION['usuario'] = null;
    }   
}
