<?php

namespace App\DTO;

class Tarefa
{
	protected $idTarefa;
	protected $titulo;
	protected $descricao;
	protected $prazoDias;
    protected $nomeResponsavel;

	public function getIdTarefa()
	{
		return $this->idTarefa;
	}

	public function setIdTarefa($idTarefa)
	{
		$this->idTarefa = $idTarefa;
		return $this;
	}
	
	public function getTitulo()
	{
		return $this->titulo;
	}

	public function setTitulo($titulo)
	{
		$this->titulo = $titulo;
		return $this;
	}
	
	public function getDescricao()
	{
		return $this->descricao;
	}

	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
		return $this;
	}
	
	public function getPrazoDias()
	{
		return $this->prazoDias;
	}

	public function setPrazoDias($prazoDias)
	{
		$this->prazoDias = $prazoDias;
		return $this;
	}

	public function getNomeResponsavel()
	{
		return $this->nomeResponsavel;
	}

	public function setNomeResponsavel($nomeResponsavel)
	{
		$this->nomeResponsavel = $nomeResponsavel;
		return $this;
	}
}
