<?php
/**
* Classe feita para auxiliar na passagem de dados de um array para um DTO equivalente
*/
namespace App\Utils;

class ArrayToEntity
{
    public static function make($array, $entity)
    {
        foreach($array as $index => $value) {
            $methodName = 'set' . self::underscoretoCamelcase($index);
            if (method_exists($entity, $methodName)) {
                $entity->$methodName($value);
            }

        }

        return $entity;
    }

    public static function underscoreToCamelcase ($value)
    {
        $words = explode('_', strtolower($value));

        $return = '';
        foreach ($words as $word) {
            $return .= ucfirst(trim($word));
        }

        return $return;
    }
}
