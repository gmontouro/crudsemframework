<?php

/**
* Classe feita para auxiliar na manipulação de Strings
*/

namespace App\Utils;

class String
{
    /**
    * Recebe uma $string em formato camelCase e o transforma em formato underscore (camel_case)
    * @param $string String
    * @return $string String
    */
    public static function toUnderscore($string)
    {
        $string=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', '_$0', $string));
        return strtolower($string);
    }
}
