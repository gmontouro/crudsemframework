<?php
/**
* Controller de Login, responsável pela exibição e tratamento das Views de Login
*/
namespace App\Controller;

class Login extends Base
{
	public function indexAction()
	{
        $usuario = \App\Session\Session::read();
        if (!empty($usuario)) {
            ob_clean();
            header('Location: /Tarefa/relatorio');
            exit();
        } 
        $this->render();
    }

    /**
    * Action responsável por fazer o logout do usuário
    */
    public function logoutAction()
    {
        \App\Session\Session::flushSession();
        ob_clean();
        header('Location: /');
        exit();
    }

    /**
    *   Action que valida o login do usuário, obrigatório receber via post usuario e senha
    *  Caso o login seja efetuado com sucesso, retorna um json com status true, se não false.
    */
    public function validateAction()
    {
        $usuario = $_POST['usuario'];
        $senha = $_POST['senha'];
        $usuarioModel = new \App\Model\Usuario();
        $usuario = $usuarioModel->findByUsuarioSenha($usuario, $senha);
        if (!empty($usuario[0])) {
            \App\Session\Session::saveSession($usuario[0]);
            echo json_encode(array('status' => true));
            exit();
        }
        echo json_encode(array('status' => false));
        exit();
    }
}
