<?php

/**
* Classe abstrata para ajudar as controllers a renderizar e encontrar o caminho correto de cada requisião
*/

namespace App\Controller;

abstract class Base
{
    // Variável que ficará armazenado as variáveis que vão para o template
    protected $data;

    /**
    * Método para assinar variáveis ao template
    * @param $index String
    * @param $value String | array()
    */
    protected function assing($index, $value)
    {
        $this->data[$index] = $value;
    }

    /**
    *  Caso não encontre o $method chamado, esta action não existe e este método avisa isso
    */
	public function __call($method, $args)
	{
		if (!method_exists($this, $method)) {
			throw new \InvalidArgumentException("action $method não existe");
		}
	}

    /**
    * Método para renderizar o layout da aplicação
    */
    protected function render()
    {
        ob_clean();
        require_once(__DIR__ . '/../views/layout.phtml');
    }
    
    /**
    * Método que encontra qual o template a ser renderizado dentro do conteúdo
    */
    protected function getContent()
    {
        $url = substr($_SERVER['REQUEST_URI'], 1);
        $args = explode("/", $url);
        if (empty($args[0])) {
            $args[0] = 'default';
        }
        if (empty($args[1])) {
            $args[1] = 'index';
        }
        require_once(__DIR__ . '/../views/' . $args[0] . '/' . $args[1] . '.phtml');
    }
    /*
    * Método que renderiza o header da página
    */
    protected function getHeader()
    {
        require_once(__DIR__ . '/../views/header.phtml');
    }

}
