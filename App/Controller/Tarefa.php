<?php

namespace App\Controller;

class Tarefa extends Base
{
    /**
    * Action que exibe a lista de tarefas em forma de tabela
    */
	public function relatorioAction()
	{
        if (\App\Session\Session::read() == null) {
            ob_clean();
            header('Location: /');
            exit();
        }
        $tarefaModel = new \App\Model\Tarefa();
        $tarefas = $tarefaModel->getAll();
        $this->assing('tarefas', $tarefas);
        $this->render();
    }

    /**
    * Action que mostra o formulário para ser preenchido e adicionar tarefas
    */
    public function incluirAction()
    {
        if (\App\Session\Session::read() == null) {
            ob_clean();
            header('Location: /');
            exit();
        }

        if (!empty($_POST)) {
            $tarefa = \App\Utils\ArrayToEntity::make($_POST, new \App\DTO\Tarefa());
            $tarefaModel = new \App\Model\Tarefa();
            $tarefaModel->save($tarefa);
            ob_clean();
            header('Location: /Tarefa/relatorio');
            exit();
        }
        $this->render();
    }

    /**
    * Action que mostra o formulário para ser preenchido e alterar a tarefa
    */
    public function editarAction()
    {
        $id = $_GET['id'];
        $tarefaModel = new \App\Model\Tarefa();
        if (!empty($_POST)) {
            $tarefa = \App\Utils\ArrayToEntity::make($_POST, new \App\DTO\Tarefa());
            $tarefa->setIdTarefa($id);
            $tarefaModel->save($tarefa);
            ob_clean();
            header('Location: /Tarefa/relatorio');
            exit();
        }
        $tarefa = $tarefaModel->find($id);
        $this->assing('tarefa', $tarefa[0]);
        $this->render();
    }
}
