<?php
/**
* Classe de manipulação de dados, responsável por enviar e receber dados do banco
*/

namespace App\Model;

use App\DTO\Usuario as UsuarioDTO;

class Usuario extends Base
{
    protected $tableName = 'usuario';

    /**
    *  retorna sempre um novo DTO em branco, para utilização em retorno do banco
    */
    public function getDTO()
    {
        return new UsuarioDTO();
    }

    /**
    * Retorna o usuario de acordo com o usuario e senha, caso não exista retorna falso
    * @param $usuario String
    * @param $senha String
    * @return array() | boolean
    */
    public function findByUsuarioSenha($usuario, $senha)
    {
        $select = new \App\Model\Query\Select();
        $select->setFrom($this->tableName);
        $select->setColumns(array('*'));
        $select->where(array('usuario' => $usuario))->where(array('senha' => $senha));
        return self::select($select);
    }
}       
