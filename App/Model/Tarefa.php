<?php
/**
* Classe de manipulação de dados, responsável por enviar e receber dados do banco
*/
namespace App\Model;

use App\DTO\Tarefa as TarefaDTO;

class Tarefa extends Base
{
    protected $tableName = 'tarefa';

    /**
    *  retorna sempre um novo DTO em branco, para utilização em retorno do banco
    */
    public function getDTO()
    {
        return new TarefaDTO();
    }

    /**
    * Salva uma tarefa no banco, se estiver sem ID cria uma nova, se estiver com ID apenas atualiza
    * @param $objeto App\DTO\Tarefa
    * @return boolean
    */
    public function save(TarefaDTO $objeto)
    {
        if ($objeto->getIdTarefa() == null) {
            return self::insert($objeto);
        }
        $where = array('id_tarefa' => $objeto->getIdTarefa());
        return self::update($objeto, $where);
    }
    
    /**
    * Remove uma tarefa a partir de seu id
    * @param $id int
    */
    public function remove($id)
    {
        self::delete(array('id_tarefa' => $id));
    }

    /**
    * retorna todas as tarefas cadastradas
    * @return array()
    */
    public function getAll()
    {
        $select = new \App\Model\Query\Select();
        $select->setFrom($this->tableName);
        $select->setColumns(array('*'));
        return self::select($select);
    }

    /**
    * Busca um item no banco diretamente pela primary key
    * @param $id int
    * @return array()
    */
    public function find($id)
    {
        $select = new \App\Model\Query\Select();
        $select->setFrom($this->tableName);
        $select->setColumns(array('*'));
        $select->where(array('id_tarefa' => $id));
        return self::select($select);
    }
}       
