<?php

/**
* Classe genérica para criação de um select completo
*/

namespace App\Model\Query;

class Select
{
    protected $from;
    protected $columns;
    protected $where;
    protected $join;
    
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }

    /**
    * Adiciona claúsulas where
    * @param $where array();
    * @return $this
    */
    public function where($where)
    {
        $this->where[] = $where;
        return $this;
    }

    /**
    * Adiciona claúsulas join
    * @param $join array();
    * @return $this
    */
    public function join($join)
    {
        $this->join[] = $join;
        return $this;
    }

    /**
    *  Cria a string SQL de acordo com os parametros que ele contém até aqui
    *  @return $string String
    */
    public function __toString()

    {
        $string = 'SELECT '.implode(',', $this->columns).' from ' . $this->from;

        if (!empty($this->join)) {
            $string.= ' INNER JOIN ' . $this->join[0] . ' on ' . $this->join[1];
        }

        if (!empty($this->where)) {
            $string.=' WHERE ';
            foreach ($this->where as $where) {
                foreach ($where as $key => $value) {
                    if (is_int($value)) {
                        $string.= $key . ' = '.$value . ' AND ';
                    } else {
                        $string.= $key . ' = \''.$value . '\' AND ';
                    }
                }
            }
            $string = substr($string,0,-4);
        }
        
        return $string;
    }
}
