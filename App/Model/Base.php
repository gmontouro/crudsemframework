<?php

/**
* Class abstrata para manipulação de dados com o banco
* deve ser extendida por todas as models
*/

namespace App\Model;

use App\Utils\String;
use App\Utils\ArrayToEntity;
use App\Model\Query\Select;

abstract class Base
{
    protected $connection;

    /**
    *   Método para pegar a conexão, caso não exista ele cria uma
    *   @return PDO
    */
    protected function getConnection()
    {
        if (empty($this->connection)) {
            $config = \Config::get('db');
            $this->connection = new \PDO ( 'mysql:host='.$config["host"].';dbname='.$config["database"], $config['user'], $config["password"]);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

        return $this->connection;
    }

    /**
    * Método genérico para inserção de dados na tabela a partir de um DTO
    * @param $objeto, DTO
    * @return boolean
    */
    protected function insert($objeto)
    {
        //Começando a string de inserção
        $string = 'INSERT INTO ' . $this->tableName .' ';
        $fields = array();

        $methods = get_class_methods($objeto);

        //Recuperar todos os campos e valores apartir dos métodos 'get' do objeto
        foreach ($methods as $method) {
            if (substr($method,0,3) == 'get') {
                if ($objeto->$method() != null) {
                    $fields[String::toUnderscore(substr($method,3))] = $objeto->$method();
                }
            }
        }
        
        //Cria um array com chaves especiais para o statemente, com seus respectivos valores
        $values = array();
        foreach ($fields as $key => $field) {
            $values[':'.$key] = $field;
        }
        
        // Termina de criar a string com campos e valores
        $string.= "(".implode(',', array_keys($fields)).") VALUES (:".implode(',:', array_keys($fields)).")";

        $statement = $this->getConnection()->prepare($string);
        
        try {
            return $statement->execute($values);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
    * Método genérico para atualização de dados a partir de um DTO
    * @param $objeto, DTO
    * @param $where, array()
    * @return boolean
    */
    protected function update($objeto, $where)
    {
        //Começando string SQL
        $string = 'UPDATE ' . $this->tableName .' ';
        $fields = array();

        $methods = get_class_methods($objeto);
        
        //Recuperar todos os campos e valores apartir dos métodos 'get' do objeto
        foreach ($methods as $method) {
            if (substr($method,0,3) == 'get') {
                if ($objeto->$method() != null) {
                    $fields[String::toUnderscore(substr($method,3))] = $objeto->$method();
                }
            }
        }

        //Preenche a string setando os campos que serão alterados
        $string.= ' SET ';
        foreach ($fields as $key => $field) {
            $string.= $key . '=?, ';
        }

        $string = substr($string,0,-2);
        
        //Preenche a clausula where, caso exista
        if (!empty($where)) {
            $string.=' WHERE ';
            foreach ($where as $key => $value) {
                $string.= $key.'=?';
                $fields[] = $value;
            }
        }
        
        //Retira apenas os valores do array $fields para passar ao statement
        $fields = array_values($fields);
        $statement = $this->getConnection()->prepare($string);
        
        try {
            return $statement->execute($fields);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
    * Método genérico para deletar dados a partir de uma condição
    * @param $where, array()
    * @return boolean
    */
    protected function delete($where)
    {
        //Começa a string SQL
        $string = 'DELETE FROM ' . $this->tableName .' ';
        
        //Cria a clausula WHERE para deletar, caso exista

        if (!empty($where)) {
            $string.=' WHERE ';
            foreach ($where as $key => $value) {
                $string.= $key.'=?';
            }
        }

        //Retira apenas os valores para o novo array
        $fields = array_values($where);
        
        $statement = $this->getConnection()->prepare($string);
        
        try {
            return $statement->execute($fields);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
    *   Método genérico para selecionar itens no banco de dados
    *   @param $select App\Model\Query\Select
    *   @return array()
    */
    public function select(Select $select)
    {
        $query = $select->__toString();
        $stmt = $this->getConnection()->prepare($query);
        $stmt->execute();
        $objects = array();

        while ($each = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $dto = ArrayToEntity::make($each, $this->getDTO());
            $objects[] = $dto;
        }

        return $objects;
    }

}
