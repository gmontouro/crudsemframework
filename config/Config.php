<?php
/**
* Classe responsável por repasser os configs declarados no começo da aplicação
*/

class Config
{
    public static function get($index)
    {
        return self::getConfig()[$index];
    }

    public static function getConfig()
    {
        return include('geral.php');
    }
}
