SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `teste` DEFAULT CHARACTER SET utf8 ;
USE `teste` ;

	-- -----------------------------------------------------
	-- Table `teste`.`tarefa`
	-- -----------------------------------------------------
	CREATE  TABLE IF NOT EXISTS `teste`.`tarefa` (
			`id_tarefa` INT NOT NULL AUTO_INCREMENT ,
			`titulo` VARCHAR(100) NOT NULL ,
			`descricao` TEXT NOT NULL ,
			`prazo_dias` INT NOT NULL ,
			`nome_responsavel` VARCHAR(100) NOT NULL ,
			PRIMARY KEY (`id_tarefa`)
			)
	ENGINE = InnoDB;

	-- -----------------------------------------------------
	-- Table `teste`.`usuario`
	-- -----------------------------------------------------
	CREATE  TABLE IF NOT EXISTS `teste`.`usuario` (
			`id_usuario` INT NOT NULL AUTO_INCREMENT ,
			`nome` VARCHAR(150) NOT NULL ,
			`usuario` VARCHAR(100) NOT NULL ,
			`senha` VARCHAR(30) NOT NULL ,
			PRIMARY KEY (`id_usuario`)
			)
	ENGINE = InnoDB;

INSERT INTO `usuario` (id_usuario, nome, usuario, senha)VALUES(1, 'Guilherme Pinheiro', 'guiga', 'guiga123') ON DUPLICATE KEY UPDATE id_usuario = 1;
INSERT INTO `usuario` (id_usuario, nome, usuario, senha)VALUES(2, 'Davi Sclifo Zucon', 'davi', 'davi123') ON DUPLICATE KEY UPDATE id_usuario = 2;
INSERT INTO `usuario` (id_usuario, nome, usuario, senha)VALUES(3, 'Carlos Eduardo', 'carlos', 'carlos123') ON DUPLICATE KEY UPDATE id_usuario = 3;
